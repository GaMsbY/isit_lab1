﻿namespace ISIT
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.equally_btn = new System.Windows.Forms.Button();
            this.less = new System.Windows.Forms.Button();
            this.more = new System.Windows.Forms.Button();
            this.drawC_btn = new System.Windows.Forms.Button();
            this.drawB_btn = new System.Windows.Forms.Button();
            this.drawA_btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.C_GV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.multiply = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.plus_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.B_GV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A_GV = new System.Windows.Forms.DataGridView();
            this.alphaA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottomA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.errors_tb = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comparison_tb = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.C_GV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B_GV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A_GV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graph)).BeginInit();
            this.SuspendLayout();
            // 
            // equally_btn
            // 
            this.equally_btn.Location = new System.Drawing.Point(102, 314);
            this.equally_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.equally_btn.Name = "equally_btn";
            this.equally_btn.Size = new System.Drawing.Size(42, 46);
            this.equally_btn.TabIndex = 33;
            this.equally_btn.Text = "=";
            this.equally_btn.UseVisualStyleBackColor = true;
            this.equally_btn.Click += new System.EventHandler(this.equally_btn_Click);
            // 
            // less
            // 
            this.less.Location = new System.Drawing.Point(56, 314);
            this.less.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.less.Name = "less";
            this.less.Size = new System.Drawing.Size(42, 46);
            this.less.TabIndex = 32;
            this.less.Text = "<";
            this.less.UseVisualStyleBackColor = true;
            this.less.Click += new System.EventHandler(this.less_Click);
            // 
            // more
            // 
            this.more.Location = new System.Drawing.Point(9, 314);
            this.more.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.more.Name = "more";
            this.more.Size = new System.Drawing.Size(42, 46);
            this.more.TabIndex = 31;
            this.more.Text = ">";
            this.more.UseVisualStyleBackColor = true;
            this.more.Click += new System.EventHandler(this.more_Click);
            // 
            // drawC_btn
            // 
            this.drawC_btn.Location = new System.Drawing.Point(195, 113);
            this.drawC_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.drawC_btn.Name = "drawC_btn";
            this.drawC_btn.Size = new System.Drawing.Size(104, 39);
            this.drawC_btn.TabIndex = 30;
            this.drawC_btn.Text = "Построить C";
            this.drawC_btn.UseVisualStyleBackColor = true;
            this.drawC_btn.Click += new System.EventHandler(this.drawC_btn_Click);
            // 
            // drawB_btn
            // 
            this.drawB_btn.Location = new System.Drawing.Point(195, 67);
            this.drawB_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.drawB_btn.Name = "drawB_btn";
            this.drawB_btn.Size = new System.Drawing.Size(104, 39);
            this.drawB_btn.TabIndex = 29;
            this.drawB_btn.Text = "Построить B";
            this.drawB_btn.UseVisualStyleBackColor = true;
            this.drawB_btn.Click += new System.EventHandler(this.drawB_btn_Click);
            // 
            // drawA_btn
            // 
            this.drawA_btn.Location = new System.Drawing.Point(195, 24);
            this.drawA_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.drawA_btn.Name = "drawA_btn";
            this.drawA_btn.Size = new System.Drawing.Size(104, 39);
            this.drawA_btn.TabIndex = 28;
            this.drawA_btn.Text = "Построить А";
            this.drawA_btn.UseVisualStyleBackColor = true;
            this.drawA_btn.Click += new System.EventHandler(this.drawA_btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 367);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Множество C:";
            // 
            // C_GV
            // 
            this.C_GV.AllowUserToAddRows = false;
            this.C_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.C_GV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.C_GV.Location = new System.Drawing.Point(9, 384);
            this.C_GV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.C_GV.Name = "C_GV";
            this.C_GV.RowTemplate.Height = 24;
            this.C_GV.Size = new System.Drawing.Size(182, 132);
            this.C_GV.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "alphaA";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "Нижняя";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.HeaderText = "Верхняя";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(148, 262);
            this.multiply.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(42, 46);
            this.multiply.TabIndex = 25;
            this.multiply.Text = "*";
            this.multiply.UseVisualStyleBackColor = true;
            this.multiply.Click += new System.EventHandler(this.multiply_Click);
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(102, 262);
            this.divide.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(42, 46);
            this.divide.TabIndex = 24;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            this.divide.Click += new System.EventHandler(this.divide_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(56, 262);
            this.minus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(42, 46);
            this.minus.TabIndex = 23;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // plus_btn
            // 
            this.plus_btn.Location = new System.Drawing.Point(9, 262);
            this.plus_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plus_btn.Name = "plus_btn";
            this.plus_btn.Size = new System.Drawing.Size(42, 46);
            this.plus_btn.TabIndex = 22;
            this.plus_btn.Text = "+";
            this.plus_btn.UseVisualStyleBackColor = true;
            this.plus_btn.Click += new System.EventHandler(this.plus_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 126);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Множество В:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Множество А:";
            // 
            // B_GV
            // 
            this.B_GV.AllowUserToAddRows = false;
            this.B_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.B_GV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.B_GV.Location = new System.Drawing.Point(9, 142);
            this.B_GV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.B_GV.Name = "B_GV";
            this.B_GV.RowTemplate.Height = 24;
            this.B_GV.Size = new System.Drawing.Size(182, 115);
            this.B_GV.TabIndex = 19;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "alphaA";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Нижняя";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Верхняя";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // A_GV
            // 
            this.A_GV.AllowUserToAddRows = false;
            this.A_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.A_GV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.alphaA,
            this.bottomA,
            this.topA});
            this.A_GV.Location = new System.Drawing.Point(9, 24);
            this.A_GV.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.A_GV.Name = "A_GV";
            this.A_GV.RowTemplate.Height = 24;
            this.A_GV.Size = new System.Drawing.Size(182, 98);
            this.A_GV.TabIndex = 18;
            // 
            // alphaA
            // 
            this.alphaA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.alphaA.HeaderText = "alphaA";
            this.alphaA.Name = "alphaA";
            // 
            // bottomA
            // 
            this.bottomA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.bottomA.HeaderText = "Нижняя";
            this.bottomA.Name = "bottomA";
            // 
            // topA
            // 
            this.topA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.topA.HeaderText = "Верхняя";
            this.topA.Name = "topA";
            // 
            // graph
            // 
            chartArea3.Name = "ChartArea1";
            this.graph.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.graph.Legends.Add(legend3);
            this.graph.Location = new System.Drawing.Point(303, 7);
            this.graph.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.graph.Name = "graph";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.graph.Series.Add(series3);
            this.graph.Size = new System.Drawing.Size(421, 299);
            this.graph.TabIndex = 35;
            this.graph.Text = "chart1";
            // 
            // errors_tb
            // 
            this.errors_tb.Location = new System.Drawing.Point(195, 384);
            this.errors_tb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.errors_tb.Name = "errors_tb";
            this.errors_tb.Size = new System.Drawing.Size(530, 44);
            this.errors_tb.TabIndex = 36;
            this.errors_tb.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(195, 365);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "Ошибки:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(195, 453);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Результаты сравнения:";
            // 
            // comparison_tb
            // 
            this.comparison_tb.Location = new System.Drawing.Point(195, 472);
            this.comparison_tb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comparison_tb.Name = "comparison_tb";
            this.comparison_tb.Size = new System.Drawing.Size(530, 44);
            this.comparison_tb.TabIndex = 38;
            this.comparison_tb.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 533);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comparison_tb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.errors_tb);
            this.Controls.Add(this.graph);
            this.Controls.Add(this.equally_btn);
            this.Controls.Add(this.less);
            this.Controls.Add(this.more);
            this.Controls.Add(this.drawC_btn);
            this.Controls.Add(this.drawB_btn);
            this.Controls.Add(this.drawA_btn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.C_GV);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.B_GV);
            this.Controls.Add(this.A_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "LAB #1 Хнюнин и Довыденко";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.C_GV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B_GV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A_GV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graph)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button equally_btn;
        private System.Windows.Forms.Button less;
        private System.Windows.Forms.Button more;
        private System.Windows.Forms.Button drawC_btn;
        private System.Windows.Forms.Button drawB_btn;
        private System.Windows.Forms.Button drawA_btn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView C_GV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button plus_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView B_GV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView A_GV;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaA;
        private System.Windows.Forms.DataGridViewTextBoxColumn bottomA;
        private System.Windows.Forms.DataGridViewTextBoxColumn topA;
        private System.Windows.Forms.DataVisualization.Charting.Chart graph;
        private System.Windows.Forms.RichTextBox errors_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox comparison_tb;
    }
}

