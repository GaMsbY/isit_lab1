﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ISIT
{
    struct Row
    {
        public double alpha;
        public double bottom;
        public double top;
    }

    struct MyPoint
    {
        public double x;
        public double y;
    }

    public partial class Form1 : Form
    {
        List<Row> A;
        List<Row> B;
        List<Row> C;
        List<string> errors = new List<string>();
        List<string> resulComparison = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        //-------ПРОВЕРКИ--------------
        private bool isContains(List<Row> table, double number)
        {
            for (int i = 0; i < table.Count; i++)
            {
                if (table[i].alpha == number) return true;
            }
            return false;
        }

        private bool isConvex(List<Row> table)//Проверка на выпуклость
        {
            for (int i = table.Count - 1; i > 0; i--)
            {
                if (table[i].bottom < table[i - 1].bottom && table[i - 1].top < table[i].top)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private bool isLevel(List<Row> table)//Проверка на наличие 0 и 1 альфа среза
        {
            if (isContains(table, 0) && isContains(table, 1)) return true; else return false;
        }

        private List<Row> check(DataGridView GV, String name)
        {
            if (getTable(GV) == null)
            {
                errors.Add("В числе " + name + " не хватает данных!");
                return null;
            }
            else
            {
                List<Row> T = new List<Row>(getTable(GV));
                if (!isLevel(T) || !isConvex(T))
                {
                    errors.Add("Число " + name + " не соответствует требованиям!");
                    return null;
                }
                return T;
            }
        }

        //-------ПОЛУЧЕНИЕ ДАННЫХ-------
        private List<Row> getTable(DataGridView GV)
        {
            List<Row> result = new List<Row>();
            for (int i = 0; i < GV.RowCount; i++)
            {
                Row row = new Row();
                if (GV.Rows[i].Cells[0].Value == null || GV.Rows[i].Cells[1].Value == null || GV.Rows[i].Cells[2].Value == null)
                {
                    return null;
                }
                else
                {
                    double.TryParse(GV.Rows[i].Cells[0].Value.ToString(), out row.alpha);
                    double.TryParse(GV.Rows[i].Cells[1].Value.ToString(), out row.bottom);
                    double.TryParse(GV.Rows[i].Cells[2].Value.ToString(), out row.top);
                    result.Add(row);
                }
            }
            return result;
        }

        private List<MyPoint> getPoints(List<Row> T)
        {
            List<MyPoint> result = new List<MyPoint>();
            for (int i = 0; i < T.Count; i++)
            {
                MyPoint point = new MyPoint();
                point.x = T[i].bottom;
                point.y = T[i].alpha;
                result.Add(point);
                point.x = T[i].top;
                point.y = T[i].alpha;
                result.Add(point);
            }
            return result;
        }

        //-------ПРЕОБРАЗОВАНИЯ----------
        private double findChange(List<Row> A, List<Row> B)//Находит недостающий альфа уровень
        {
            for(int i = 0; i < B.Count; i++)
            {
                if(!isContains(A, B[i].alpha)) {
                    return B[i].alpha;
                }
            }
            return 0;///Это костыль
        }

        private List<Row> addTable(List<Row> A, List<Row> B)//Дополняет А недостающим альфа уровнем
        {
            double missed = findChange(A, B);
            for (int i = A.Count - 1; i > 0; i--)
            {
                if (A[i].alpha < missed && A[i - 1].alpha > missed)
                {
                    Row row = new Row();
                    row.alpha = missed;
                    row.bottom = -((A[i - 1].bottom - A[i].bottom) * missed + (A[i].bottom * A[i - 1].alpha + A[i - 1].bottom * A[i].alpha)) / (A[i].alpha - A[i - 1].alpha);
                    row.top = -((A[i - 1].top - A[i].top) * missed + (A[i].top * A[i - 1].alpha + A[i - 1].top * A[i].alpha)) / (A[i].alpha - A[i - 1].alpha);
                    A.Insert(i, row);
                    break;
                }
            }
            return A;
        }

        private List<MyPoint> sortPoints(List<MyPoint> points)
        {
            for (int i = 0; i < points.Count; i++)
            {
                for (int j = i + 1; j < points.Count; j++)
                {
                    if (points[i].x > points[j].x)
                    {
                        MyPoint buf = points[i];
                        points[i] = points[j];
                        points[j] = buf;
                    }
                }
            }
            return points;
        }

        //----ОПЕРАЦИИ------
        private List<Row> action(string action)//арифметические операции
        {
            A = check(A_GV, "A");
            B = check(B_GV, "B");
            if (A == null || B == null) return null;
           
            if (isLevel(A) && isConvex(A) && isLevel(B) && isConvex(B))
            {
                A = new List<Row>(addTable(A, B));
                C = new List<Row>();
                switch (action)
                {
                    case "plus":
                        {
                            for (int i = 0; i < B.Count; i++)
                            {
                                Row row = new Row();
                                row.alpha = B[i].alpha;
                                row.bottom = A[i].bottom + B[i].bottom;
                                row.top = A[i].top + B[i].top;
                                C.Add(row);
                            }
                            break;
                        }

                    case "multiply":
                        {
                            for (int i = 0; i < B.Count; i++)
                            {
                                Row row = new Row();
                                row.alpha = B[i].alpha;
                                row.bottom = A[i].bottom * B[i].bottom;
                                row.top = A[i].top * B[i].top;
                                C.Add(row);
                            }
                            break;
                        }

                    case "minus":
                        {
                            for (int i = 0; i < B.Count; i++)
                            {
                                Row row = new Row();
                                row.alpha = B[i].alpha;
                                row.bottom = A[i].bottom - B[i].top;
                                row.top = A[i].top - B[i].bottom;
                                C.Add(row);
                            }
                            break;
                        }

                    case "divide":
                        {
                            for (int i = 0; i < B.Count; i++)
                            {
                                Row row = new Row();
                                row.alpha = B[i].alpha;
                                if(B[i].top == 0 || B[i].bottom == 0)
                                {
                                    errors.Add("Деление на 0!");
                                    C.Clear();
                                    break;
                                }
                                row.bottom = A[i].bottom / B[i].top;
                                row.top = A[i].top / B[i].bottom;
                                C.Add(row);
                            }
                            break;
                        }
                }
                return C;
            }
            else return null;
        }

        private double sum(List<Row> T)
        {
            double sum = 0;
            for (int i = 0; i < T.Count; i++)
            {
                sum += T[i].bottom + T[i].top;
            }
            sum /= T.Count;
            return sum;
        }

        private void comparison(string action)
        {
            A = check(A_GV, "A");
            B = check(B_GV, "B");
            if (A==null || B==null)
            {
                showErrors();
                return;
            }

            double sumA = sum(A);
            double sumB = sum(B);
            
            switch (action)
            {
                case "more":
                    {
                        if(sumA == sumB)
                        {
                            resulComparison.Add("А равно В");
                        }
                        if(sumA > sumB)
                        {
                            resulComparison.Add("А больше В");
                        }
                        else
                        {
                            resulComparison.Add("В больше А");
                        }
                        break;
                    }
                case "less":
                    {
                        if (sumA == sumB)
                        {
                            resulComparison.Add("А равно В");
                        }
                        if (sumA > sumB)
                        {
                            resulComparison.Add("В меньше А");
                        }
                        else
                        {
                            resulComparison.Add("А меньше В");
                        }
                        break;
                    }
                case "equally":
                    {
                        if (sumA == sumB)
                        {
                            resulComparison.Add("А равно В");
                        }
                        else
                        {
                            resulComparison.Add("А неравно В");
                        }
                        break;
                        
                    }
            }
            showComparison();
        }

        private void action_click(String actionName)
        {
            errors_tb.Clear();
            List<Row> C = action(actionName);
            if (C != null) showTable(C, C_GV);
            showErrors();
        }

        private List<MyPoint> prepareToDraw(string nameTable)
        {
            switch (nameTable)
            {
                case "A":
                    {
                        A = check(A_GV, nameTable);
                        if (A == null) return null;
                        return sortPoints(getPoints(A));
                    }
                case "B":
                    {
                        B = check(B_GV, nameTable);
                        if (B == null) return null;
                        return sortPoints(getPoints(B));
                    }
                case "C":
                    {
                        C = check(C_GV, nameTable);
                        if (C == null) return null;
                        return sortPoints(getPoints(C));
                    }
            }
            return null;
        }

        //------ОТОБРАЖЕНИЕ В ИНТЕРФЕЙСЕ-----
        private void showErrors()
        {
            errors_tb.Clear();
            for (int i = 0; i < errors.Count; i++)
            {
                errors_tb.Text += errors[i];
            }
            errors.Clear();
        }

        private void showComparison()
        {
            comparison_tb.Clear();
            for (int i = 0; i < resulComparison.Count; i++)
            {
                comparison_tb.Text += resulComparison[i];
            }
            resulComparison.Clear();
        }

        private void showTable(List<Row> table, DataGridView GV)
        {
            for (int i = 0; i < table.Count; i++)
            {
                GV.Rows[i].Cells[0].Value = table[i].alpha;
                GV.Rows[i].Cells[1].Value = table[i].bottom;
                GV.Rows[i].Cells[2].Value = table[i].top;
            }
        }

        private void drawGraph(List<MyPoint> points, String name, Color color)
        {
            if (points != null)
            {
                if (graph.Series.FindByName(name) == null)
                {
                    graph.Series.Add(name);
                    graph.Series[name].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    graph.Series[name].Color = color;
                    graph.Series[name].BorderWidth = 3;
                }
                graph.Series[name].Points.Clear();
                for (int i = 0; i < points.Count; i++)
                {
                    graph.Series[name].Points.AddXY(points[i].x, points[i].y);
                }
            }
            showErrors();
        }

        //--------------ON-CLICK-LISTENERS--------------------------------
        private void Form1_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < 3; i++)
            {
                A_GV.Rows.Add();
            }
            for (int i = 0; i < 4; i++)
            {
                B_GV.Rows.Add();
            }
            for (int i = 0; i < 4; i++)
            {
                C_GV.Rows.Add();
            }
            graph.Series.Clear();
            foreach (DataGridViewRow row in C_GV.Rows)
                row.ReadOnly = true;

            A_GV.Rows[0].Cells[0].Value = 1;
            A_GV.Rows[0].Cells[1].Value = 5;
            A_GV.Rows[0].Cells[2].Value = 7;
            A_GV.Rows[1].Cells[0].Value = 0.5;
            A_GV.Rows[1].Cells[1].Value = 2;
            A_GV.Rows[1].Cells[2].Value = 8;
            A_GV.Rows[2].Cells[0].Value = 0;
            A_GV.Rows[2].Cells[1].Value = 0;
            A_GV.Rows[2].Cells[2].Value = 10;

            B_GV.Rows[0].Cells[0].Value = 1;
            B_GV.Rows[0].Cells[1].Value = 4;
            B_GV.Rows[0].Cells[2].Value = 5;
            B_GV.Rows[1].Cells[0].Value = 0.5;
            B_GV.Rows[1].Cells[1].Value = 3;
            B_GV.Rows[1].Cells[2].Value = 6;
            B_GV.Rows[2].Cells[0].Value = 0.25;
            B_GV.Rows[2].Cells[1].Value = 2;
            B_GV.Rows[2].Cells[2].Value = 7;
            B_GV.Rows[3].Cells[0].Value = 0;
            B_GV.Rows[3].Cells[1].Value = 0;
            B_GV.Rows[3].Cells[2].Value = 8;
        }

        private void plus_btn_Click(object sender, EventArgs e)
        {
            action_click("plus");
        }

        private void minus_Click(object sender, EventArgs e)
        {
            action_click("minus");
        }

        private void divide_Click(object sender, EventArgs e)
        {
            action_click("divide");
        }

        private void multiply_Click(object sender, EventArgs e)
        {
            action_click("multiply");
        }

        private void drawA_btn_Click(object sender, EventArgs e)
        {
            drawGraph(prepareToDraw("A"), "A", Color.Red);
        }

        private void drawB_btn_Click(object sender, EventArgs e)
        {
            drawGraph(prepareToDraw("B"), "B", Color.Blue);
        }

        private void drawC_btn_Click(object sender, EventArgs e)
        {
            drawGraph(prepareToDraw("C"), "C", Color.Yellow);
        }

        private void more_Click(object sender, EventArgs e)
        {
            comparison("more");
        }

        private void less_Click(object sender, EventArgs e)
        {
            comparison("less");
        }

        private void equally_btn_Click(object sender, EventArgs e)
        {
            comparison("equally");
        }
    }
}
